using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null) {
            instance = new SoundManager();
        } else {
            Destroy(this.gameObject);
        }
    }

    public void playMusic(AudioClip clip) {
        this.GetComponent<AudioSource>().clip = clip;
        this.GetComponent<AudioSource>().Play();
    }
}

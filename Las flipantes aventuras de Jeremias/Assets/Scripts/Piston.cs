using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piston : MonoBehaviour
{
    [SerializeField] private int moveDuration = 4;
    [SerializeField] private float moveSpeed = 0.015f;
    
    [SerializeField] private bool goUp = true;

    void Awake() {
        StartCoroutine(subirPiston());
    }

    void FixedUpdate() {
        if (goUp) {
            this.transform.position += Vector3.up * moveSpeed;
        } else {
            this.transform.position += Vector3.down * moveSpeed;
        }
    }

    IEnumerator subirPiston() {
        yield return new WaitForSeconds(moveDuration);
        goUp = !goUp;
        StartCoroutine(subirPiston());
    }

}

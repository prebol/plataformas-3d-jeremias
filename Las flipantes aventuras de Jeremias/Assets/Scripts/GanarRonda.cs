using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GanarRonda : MonoBehaviour
{
    [SerializeField] private bool haGanado = false;
    [SerializeField] private CanvasRenderer victoriaTxt, pulsaUno;

    void OnTriggerEnter(Collider col) {
        if (col.transform.CompareTag("player") && !haGanado) {
            haGanado = true;
            victoriaTxt.gameObject.SetActive(true);
            pulsaUno.gameObject.SetActive(true);
            print("EL PLAYER HA GANADO LA PARTIDA");
        }
    }
}

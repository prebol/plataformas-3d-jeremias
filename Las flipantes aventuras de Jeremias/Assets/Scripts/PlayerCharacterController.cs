using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerCharacterController : MonoBehaviour
{
    [SerializeField] private int moveSpeed = 2;
    [SerializeField] private int jumpForce = 1000;
    [SerializeField] private float rotationSpd = 1.5f;

    private Vector3 moveDirection = Vector3.zero;
    private Vector3 velocity;

    [SerializeField] private bool isGrounded;
    [SerializeField] private float groundCheckDistance;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private float gravity;

    Status status = Status.IDLE;

    Rigidbody rb;
    Animator _animator;
    CharacterController _charController;
    PlayerInputActions inputPlayer;
    private Vector2 input_movement = Vector2.zero;
    InputAction move;
    InputAction jump;

    void Awake()
    {
        inputPlayer = new PlayerInputActions();
    }

    void Start() {
        rb = GetComponent<Rigidbody>();
        _animator = GetComponent<Animator>();
        _charController = GetComponent<CharacterController>();
    }

    void OnEnable() {
        move = inputPlayer.Player.Move;
        move.Enable();

        jump = inputPlayer.Player.Jump;
        jump.Enable();
        jump.performed += IJump;

    }

    void OnDisable() {
        move.Disable();
        jump.Disable();
    }

    void Update()
    {
        Move();
        print("Status: " + status);
    }

    private void Move() {

        if (isGrounded && velocity.y < 0) {
            velocity.y = -2f;
        }

        //CHARACTER CONTROLLER
        input_movement = move.ReadValue<Vector2>();
        float moveX = input_movement.x;
        float moveZ = input_movement.y;

        moveDirection = new Vector3(0, 0, moveZ);
        moveDirection = transform.TransformDirection(moveDirection);

        if (isGrounded) {
            if (moveDirection != Vector3.zero) {
                Walk();
            } else if (moveDirection == Vector3.zero) {
                Idle();
            }

            moveDirection *= moveSpeed;
        }

        //ACTUAL Z MOVEMENT
        _charController.Move(moveDirection * Time.deltaTime);

        //Y ROTATION
        transform.Rotate(0, moveX * rotationSpd, 0);

        //CHARACTER GRAVITY
        
        velocity.y += gravity * Time.deltaTime;
        _charController.Move(velocity * Time.deltaTime);

        print($"X: {moveX}, Z: {moveZ}");
    }

    private void Walk() {
        status = Status.WALKING;
        _animator.SetBool("IsWalking",true);
        
    }

    private void Idle() {
        status = Status.IDLE;
        _animator.SetBool("IsWalking",false);
    }

    public void IJump(InputAction.CallbackContext context) {

        if (context.performed) {
            if (isGrounded) {
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                isGrounded = false;
                print("JUMP!!!!");
            }
        }
    }

    void OnCollisionEnter(Collision col) {

        if (col.transform.CompareTag("suelo")) {
            isGrounded = true;
        }
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class caca : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision col) {
        if (col.transform.CompareTag("player")) {
            col.gameObject.GetComponent<PlayerController>().slowdown = 2;
        }
    }

    void OnCollisionExit(Collision col) {
        if (col.transform.CompareTag("player")) {
            col.gameObject.GetComponent<PlayerController>().slowdown = 1;
        }
    }
}

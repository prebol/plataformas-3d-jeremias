using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public int moveSpeed;
    [SerializeField] private int jumpForce = 1000;
    [SerializeField] private float rotationSpd = 1.5f;

    [SerializeField] private int walkSpeed = 2;
    [SerializeField] private int runSpeed = 5;

    public int slowdown = 1;

    public Vector3 lastCheckpoint = new Vector3(1.77f, 6.8f, 26);

    [SerializeField] private GameObject vcamera;
    [SerializeField] private SoundManager soundManager;
    [SerializeField] private AudioClip macarena;

    private Vector3 moveDirection = Vector3.zero;

    private bool isGrounded, isRunning;

    Status status = Status.IDLE;

    //NORMAL COMPONENTS
    Rigidbody rb;
    Animator _animator;

    //INPUT SYSTEM
    PlayerInputActions inputPlayer;
    private Vector2 input_movement = Vector2.zero;
    InputAction move;
    InputAction jump;
    InputAction run;

    void Awake()
    {
        inputPlayer = new PlayerInputActions();
    }

    void Start() {
        rb = GetComponent<Rigidbody>();
        _animator = GetComponent<Animator>();

        moveSpeed = walkSpeed;
    }

    void OnEnable() {
        move = inputPlayer.Player.Move;
        move.Enable();

        jump = inputPlayer.Player.Jump;
        jump.Enable();
        jump.performed += IJump;

        run = inputPlayer.Player.Run;
        run.Enable();
        run.performed += IRun;
        run.canceled += IRun;
    }

    void OnDisable() {
        move.Disable();
        jump.Disable();
    }

    void FixedUpdate()
    {
        Move();
        CheckFall();

        MoveCameraMouse();

        DanceMacarena();

        //print("Status: " + status);
    }

    private void DanceMacarena() {

        if (Input.GetKeyDown(KeyCode.Alpha1) && (status == Status.IDLE)) {
            Macarena();
        }
    }

    void ShowAndUnlockCursor()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    void HideAndLockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }


    private void MoveCameraMouse() {
        if (Input.GetKeyDown(KeyCode.F)) {
            HideAndLockCursor();
        }
        if (Input.GetKeyDown(KeyCode.G)) {
            ShowAndUnlockCursor();
        }


        /*Vector3 NewPosition = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);
        
        if (NewPosition.x > 0.0f) {
            vcamera.transform.Rotate(0, 1 * rotationSpd, 0);
        }*/
        
        
        //print("Mouse X: " + NewPosition.x + ", Mouse Y: " + NewPosition.y);

    }

    private void CheckFall() {

        if (rb.velocity.y < 0 && !isGrounded) {
            Fall(true);
        } else if (rb.velocity.y == 0) {
            Fall(false);
        }
    }

    private void Move() {

        //MOVE BY VECTORS
        input_movement = move.ReadValue<Vector2>();
        float moveX = input_movement.x;
        float moveZ = input_movement.y;

        if (moveZ > 0) {
            moveDirection += this.transform.forward;
        }
        if (moveZ < 0) {
            moveDirection += -this.transform.forward;
        }

        rb.velocity = new Vector3(0, rb.velocity.y, 0);
        rb.velocity += moveDirection * (moveSpeed / slowdown);

        if (isGrounded) {
            if (moveDirection != Vector3.zero) {
                _animator.SetBool("DanceMacarena",value: false);
                soundManager.playMusic(null);
                Walk();
            } else if ((moveDirection == Vector3.zero) && (status != Status.MACARENA)) {
                Idle();
            }
        }

        //Y ROTATION
        this.transform.Rotate(0, moveX * rotationSpd, 0);

        moveDirection = Vector3.zero;

        //print($"X: {moveX}, Z: {moveZ}");
    }

    private void Walk() {
        status = Status.WALKING;
        _animator.SetBool("IsWalking",true);
    }

    private void Idle() {
        status = Status.IDLE;
        _animator.SetBool("IsWalking",false);
    }

    private void Fall(bool fall) {
        if (fall) {
            status = Status.FALL;
            _animator.SetBool("Falling",true);
        } else {
            status = Status.IDLE;
            _animator.SetBool("Falling",false);
        }
        
    }

    private void Jump() {
        status = Status.JUMPING;
        _animator.SetBool("Jump",value: true);
    }

    private void Run(bool run) {
        if (run) {
            status = Status.RUNNING;
            _animator.SetBool("IsRunning",true);
        } else {
            _animator.SetBool("IsRunning",false);
        }
    }

    private void Macarena() {
        status = Status.MACARENA;
        _animator.SetBool("DanceMacarena",value: true);
        soundManager.playMusic(macarena);
    }

    public void IJump(InputAction.CallbackContext context) {

        if (context.performed) {
            if (isGrounded) {
                Jump();
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                //print("JUMP!!!!");
            }
        }
    }

    public void IRun(InputAction.CallbackContext context) {

        if (context.performed) {
            if (isGrounded) {
                isRunning = true;
                Run(true);
                moveSpeed = runSpeed;
                //print("RUNNING!!!!");
            }
        }
        if (context.canceled) {
            isRunning = false;
            Run(false);
            moveSpeed = walkSpeed;
            //ººprint("STOPED RUNNING!!!!");
        }
    }

    void OnCollisionEnter(Collision col) {

        if (col.transform.CompareTag("suelo")) {
            isGrounded = true;
            Fall(false);
            _animator.SetBool("IsGrounded",true);
            _animator.SetBool("Jump",false);
        }

        if (col.transform.CompareTag("checkpoint")) {   //PARA GUARDAR EL ULTIMO CHECKPOINT AL QUE HA LLEGADO EL JUGADOR
            this.lastCheckpoint = col.gameObject.transform.position;
        }
    }

    void OnCollisionStay(Collision col) {
        if (col.transform.CompareTag("suelo")) {
            isGrounded = true;
            Fall(false);
            _animator.SetBool("IsGrounded",true);
            _animator.SetBool("Jump",false);
        }
    }

    void OnCollisionExit(Collision col) {

        if (col.transform.CompareTag("suelo")) {
            isGrounded = false;
        }
    }
}

public enum Status {
    IDLE, WALKING, RUNNING, JUMPING, FALL, MACARENA
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class controlCharManager : MonoBehaviour
{
    private CharacterController _charController;
    private Animator _animator;

    private float inputX;
    private float inputZ;
    private Vector3 v_movement;
    private Vector3 v_velocity;
    private float moveSpeed;
    private float gravity;

    PlayerInputActions inputPlayer;
    InputAction move;
    Vector3 moveDirection = Vector3.zero;
    InputAction jump;
    // Start is called before the first frame update
    void Start()
    {
        GameObject tempPlayer = GameObject.FindGameObjectWithTag("Player");
        _charController = tempPlayer.GetComponent<CharacterController>();
        _animator = tempPlayer.GetComponent<Animator>();

        inputPlayer = new PlayerInputActions();

        moveSpeed = 3;
        gravity = 0.5f;
    }

    void OnEnable() {
        move = inputPlayer.Player.Move;
        move.Enable();

        jump = inputPlayer.Player.Jump;
        jump.Enable();

    }

    void OnDisable() {
        move.Disable();

        jump.Disable();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        /*moveDirection = move.ReadValue<Vector2>();

        Vector3 v_movement = _charController.transform.forward * moveDirection.y;

        _charController.transform.Rotate(Vector3.up * moveDirection.x * (100f * Time.deltaTime));

        _charController.Move(v_movement * moveSpeed * Time.deltaTime);*/
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piston_suelo : MonoBehaviour
{
    private GameObject piston;
    // Start is called before the first frame update
    void Start()
    {
        piston = this.transform.parent.gameObject;
    }

    void OnCollisionEnter(Collision col) {
        if (col.transform.CompareTag("player")) {
            col.gameObject.transform.SetParent(piston.transform);
        }
    }

    void OnCollisionExit(Collision col) {
        if (col.transform.CompareTag("player")) {
            col.gameObject.transform.SetParent(null);
        }
    }
}

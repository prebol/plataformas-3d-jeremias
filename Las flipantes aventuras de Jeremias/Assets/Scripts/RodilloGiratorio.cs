using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RodilloGiratorio : MonoBehaviour
{
    void Update()
    {
        this.transform.Rotate(0, 1, 0);
    }

    void OnCollisionStay(Collision col) {
        if (col.transform.CompareTag("player")) {
            col.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.right * 1000);
        }
    }
}

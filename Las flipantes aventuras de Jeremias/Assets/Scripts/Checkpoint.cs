using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private Vector3 spawnPoint;
    [SerializeField] private bool checkpointActivado = false;

    void Awake() {
        spawnPoint = transform.Find("spawnpoint").transform.position;
    }
    
    void OnTriggerEnter(Collider col) {
        if (col.transform.CompareTag("player") && !checkpointActivado) {
            col.gameObject.GetComponent<PlayerController>().lastCheckpoint = spawnPoint;
            checkpointActivado = true;
            print("Player arrived to checkpoint: " + this.transform.name);
        }
    }
}

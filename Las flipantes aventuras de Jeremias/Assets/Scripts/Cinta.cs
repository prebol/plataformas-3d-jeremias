using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cinta : MonoBehaviour
{
    void OnCollisionStay(Collision col) {
        if (col.transform.CompareTag("player")) {
            col.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.back * 1500);
        }
    }
}
